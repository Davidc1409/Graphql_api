const express = require("express");
const {GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLInt,
    GraphQLNonNull} = require("graphql");
const expressGraphql = require('express-graphql').graphqlHTTP;
const app = express();
const port =4900;
const { Client } = require('pg');



const client = new Client({
    user: 'postgres',
    host: 'localhost', 
    database: 'tirok', 
    password: "", 
    port: 5432, 
  });
  client.connect();


const SmartContractType= new GraphQLObjectType ({
    name : "SmartContract",
    fields:{
        id: { type: GraphQLInt },
        contract_name: { type: GraphQLString },
        contract_address: { type: GraphQLString },
        token_balance : { type: GraphQLInt },
    }
})

const projectType= new GraphQLObjectType ({
    name : "project",
    fields:{
        id: { type: GraphQLInt },
        contract_id : {type:GraphQLInt},
        project_name: { type: GraphQLString },
        project_description: { type: GraphQLString },
        rent : {type:GraphQLInt}
    }
})

const tokenType= new GraphQLObjectType({
    name : "token",
    fields:{
        contract_id : {type:GraphQLString},
        project_id: {type: GraphQLString },
        token_amount : {type:GraphQLInt}
    }
})

const walletType= new GraphQLObjectType({
    name : "wallet",
    fields:{
        id : {type:GraphQLInt},
        name : {type: GraphQLString},
        private_key: {type: GraphQLString },
        contract_id : {type: GraphQLInt},
        balance : {type:GraphQLInt}
    }
})

const QueryType = new GraphQLObjectType ({
    name : "query",
    fields: () => ({
        SmartContract:{
            type: SmartContractType,
            description : "one contract",
            args : {
                id: {type: GraphQLNonNull(GraphQLInt)}
            },
            resolve: async (parent, args) => {
            const res= await client.query('SELECT * FROM contract WHERE id=$1',[args.id]);
            console.log(res.rows[0])
            return res.rows[0];
            }
        },
        SmartContractList:{
            type: new GraphQLList(SmartContractType),
            description: 'List of All contracts',
            resolve: async () => {
            const res= await client.query('SELECT * FROM contract');
            return res.rows;
            }
        }
        
    })
    
})

const MutationType= new GraphQLObjectType ({
    name: "mutation",
    fields:()=>({
        AddSmartContract : {
            type : SmartContractType,
            description: "Add a smart contract",
            args: {
                contract_name: { type: GraphQLString },
                contract_address: { type: GraphQLString },

            },
            resolve: async (parent,args)=>{
               const res= await client.query('INSERT INTO contract (contract_name,contract_address) VALUES ($1, $2) RETURNING *',[args.contract_name,args.contract_address]);
               return res.rows[0];
            }
        },
        AddProject : {
            type : projectType,
            description: "Add a project",
            args: {
                contract_id : {type:GraphQLInt},
                project_name: { type: GraphQLString },
                project_description: { type: GraphQLString },
            },
            resolve: async (parent,args)=>{
               const res= await client.query('INSERT INTO project (contract_id,project_name,project_description) VALUES ($1, $2,$3) RETURNING *',[args.contract_id,args.project_name,args.project_description]);
               return res.rows[0];
            }
        },
        Addwallet : {
            type : walletType,
            description: "Add a wallet",
            args: {
                name : {type: GraphQLString},
                contract_id : {type: GraphQLInt},
                balance : {type:GraphQLInt}
            },
            resolve: async (parent,args)=>{
               const res= await client.query('INSERT INTO wallet (name,contract_id,balance) VALUES ($1, $2,$3) RETURNING *',[args.name,args.contract_id,args.balance]);
               return res.rows[0];
            }
        },
        GiveRent : {
            type : new GraphQLList(walletType),
            description: "give rent to token owners",
            args: {
                price: { type: GraphQLInt },
                project_id: {type: GraphQLInt},
            },
            resolve: async (parent,args)=>{
               const token_amount= await client.query('SELECT token_amount, contract_id FROM token WHERE project_id=$1',[args.project_id]);
               let walletList=[];
               for(i=0;i<token_amount.rows.length;i++){
                    let total_rent=(token_amount.rows[i].token_amount*args.price)*0.01;
                    const payout= await client.query("UPDATE wallet SET balance=(balance+$1) WHERE contract_id=$2 RETURNING *",[total_rent,token_amount.rows[i].contract_id]);
                    walletList.push(payout.rows[0]);
                }
                return walletList;
            }
        },
        BuyToken: {
            type : SmartContractType,
            description: "acheter des token",
            args : {
                project_id:{type:GraphQLNonNull(GraphQLInt)},
                contract_id_from: {type:GraphQLNonNull(GraphQLInt)},
                token_amount : {type:GraphQLNonNull(GraphQLInt)},
                contract_id_to : {type:GraphQLNonNull(GraphQLInt)},
                price: {type:GraphQLNonNull(GraphQLInt)},
            },
            resolve : async (parent,args)=>{

                const wallet_money= await client.query('SELECT balance FROM wallet WHERE contract_id=$1',[args.contract_id_to]);
                const money_in_wallet=wallet_money.rows[0].balance;
                
                if(money_in_wallet<args.price*args.token_amount){
                    return console.log("not enough money, you need to recharge !");
                }
                
                const total=args.price*args.token_amount;
                transfer_out(args.token_amount,args.contract_id_from,args.project_id);
                pay_out(total,args.contract_id_from);
                
                const already_have_token= await client.query('SELECT * FROM token WHERE contract_id=$1 AND project_id=$2',[args.contract_id_to,args.project_id]);
                const check=already_have_token.rows[0];

                if(!check){
    
                    transfer_in (args.contract_id_to,args.project_id,args.token_amount);
                    pay_in(total, args.contract_id_to);
                    return update_token_balance (args.token_amount,args.contract_id_to);
                    
                }
                else{
                    
                    const new_balance_to= await client.query('UPDATE token SET token_amount=(token_amount+$1) WHERE contract_id=$2 AND project_id=$3 RETURNING *',[args.token_amount, args.contract_id_to, args.project_id]);

                    pay_in(total, args.contract_id_to);
                    return update_token_balance (args.token_amount,args.contract_id_to);

                 }     
            }
        }
        
    })
})


async function transfer_out(token_amount,contract_id_from, project_id){
    const token_sold= await client.query("UPDATE token SET token_amount=(token_amount-$1) WHERE contract_id=$2 AND project_id=$3 RETURNING *",[token_amount, contract_id_from, project_id]);

    const new_total_balance_from= await client.query('UPDATE contract SET token_balance=(token_balance-$1) WHERE id=$2 RETURNING *',[token_amount,contract_id_from]);
}

async function pay_out(total,contract_id_from){
    const payout= await client.query("UPDATE wallet SET balance=(balance+$1) WHERE contract_id=$2 RETURNING *",[total,contract_id_from])
}

async function transfer_in (contract_id_to,project_id,token_amount){
    const res= await client.query('INSERT INTO token (contract_id,project_id,token_amount) VALUES ($1, $2,$3) RETURNING *',[contract_id_to,project_id,token_amount]);
}

async function pay_in (total,contract_id_to){
    const payin= await client.query("UPDATE wallet SET balance=(balance-$1) WHERE contract_id=$2 RETURNING *",[total, contract_id_to]);
}

async function update_token_balance (token_amount, contract_id_to){
    const new_total_balance_to= await client.query('UPDATE contract SET token_balance=(token_balance+$1) WHERE id=$2 RETURNING *',[token_amount,contract_id_to]);
    return new_total_balance_to.rows[0];
}

const schema = new GraphQLSchema({ 
    query: QueryType, 
    mutation: MutationType 
});

app.use('/graphql',expressGraphql({
    schema,
    graphiql:true
}))

app.listen(port, ()=>{
    console.log("listenning to "+port);
})

