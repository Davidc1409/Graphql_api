PGDMP                       {           tirok    16.0    16.0 ;               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                        1262    16398    tirok    DATABASE     x   CREATE DATABASE tirok WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'French_France.utf8';
    DROP DATABASE tirok;
                postgres    false            �            1259    16401    contract    TABLE     �   CREATE TABLE public.contract (
    id integer NOT NULL,
    contract_name character varying(255) NOT NULL,
    contract_address character varying(255) NOT NULL,
    token_balance integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.contract;
       public         heap    postgres    false            �            1259    16399    contract_id_seq    SEQUENCE     �   CREATE SEQUENCE public.contract_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.contract_id_seq;
       public          postgres    false    216            !           0    0    contract_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.contract_id_seq OWNED BY public.contract.id;
          public          postgres    false    215            �            1259    16461    project    TABLE     �   CREATE TABLE public.project (
    id integer NOT NULL,
    contract_id integer NOT NULL,
    project_name character varying NOT NULL,
    project_description character varying,
    rent integer DEFAULT 0
);
    DROP TABLE public.project;
       public         heap    postgres    false            �            1259    16467    project_contractId_seq    SEQUENCE     �   CREATE SEQUENCE public."project_contractId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."project_contractId_seq";
       public          postgres    false    224            "           0    0    project_contractId_seq    SEQUENCE OWNED BY     T   ALTER SEQUENCE public."project_contractId_seq" OWNED BY public.project.contract_id;
          public          postgres    false    225            �            1259    16460    project_id_seq    SEQUENCE     �   CREATE SEQUENCE public.project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.project_id_seq;
       public          postgres    false    224            #           0    0    project_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.project_id_seq OWNED BY public.project.id;
          public          postgres    false    223            �            1259    16499    project_rent_seq    SEQUENCE     �   CREATE SEQUENCE public.project_rent_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.project_rent_seq;
       public          postgres    false    224            $           0    0    project_rent_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.project_rent_seq OWNED BY public.project.rent;
          public          postgres    false    227            �            1259    16453    token    TABLE     �   CREATE TABLE public.token (
    contract_id integer NOT NULL,
    project_id integer NOT NULL,
    token_amount integer NOT NULL
);
    DROP TABLE public.token;
       public         heap    postgres    false            �            1259    16451    token_contractId_seq    SEQUENCE     �   CREATE SEQUENCE public."token_contractId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."token_contractId_seq";
       public          postgres    false    222            %           0    0    token_contractId_seq    SEQUENCE OWNED BY     P   ALTER SEQUENCE public."token_contractId_seq" OWNED BY public.token.contract_id;
          public          postgres    false    220            �            1259    16452    token_projectId_seq    SEQUENCE     �   CREATE SEQUENCE public."token_projectId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."token_projectId_seq";
       public          postgres    false    222            &           0    0    token_projectId_seq    SEQUENCE OWNED BY     N   ALTER SEQUENCE public."token_projectId_seq" OWNED BY public.token.project_id;
          public          postgres    false    221            �            1259    16507    token_tokenAmount_seq    SEQUENCE     �   CREATE SEQUENCE public."token_tokenAmount_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public."token_tokenAmount_seq";
       public          postgres    false    222            '           0    0    token_tokenAmount_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public."token_tokenAmount_seq" OWNED BY public.token.token_amount;
          public          postgres    false    228            �            1259    16429    wallet    TABLE     �   CREATE TABLE public.wallet (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    private_key character varying(255) DEFAULT '13651528723'::bigint NOT NULL,
    contract_id integer NOT NULL,
    balance integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.wallet;
       public         heap    postgres    false            �            1259    16490    wallet_balance_seq    SEQUENCE     �   CREATE SEQUENCE public.wallet_balance_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.wallet_balance_seq;
       public          postgres    false    218            (           0    0    wallet_balance_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.wallet_balance_seq OWNED BY public.wallet.balance;
          public          postgres    false    226            �            1259    16437    wallet_contractId_seq    SEQUENCE     �   CREATE SEQUENCE public."wallet_contractId_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public."wallet_contractId_seq";
       public          postgres    false    218            )           0    0    wallet_contractId_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public."wallet_contractId_seq" OWNED BY public.wallet.contract_id;
          public          postgres    false    219            �            1259    16428    wallet_id_seq    SEQUENCE     �   CREATE SEQUENCE public.wallet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.wallet_id_seq;
       public          postgres    false    218            *           0    0    wallet_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.wallet_id_seq OWNED BY public.wallet.id;
          public          postgres    false    217            e           2604    16404    contract id    DEFAULT     j   ALTER TABLE ONLY public.contract ALTER COLUMN id SET DEFAULT nextval('public.contract_id_seq'::regclass);
 :   ALTER TABLE public.contract ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    216    216            n           2604    16464 
   project id    DEFAULT     h   ALTER TABLE ONLY public.project ALTER COLUMN id SET DEFAULT nextval('public.project_id_seq'::regclass);
 9   ALTER TABLE public.project ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    223    224            o           2604    16468    project contract_id    DEFAULT     {   ALTER TABLE ONLY public.project ALTER COLUMN contract_id SET DEFAULT nextval('public."project_contractId_seq"'::regclass);
 B   ALTER TABLE public.project ALTER COLUMN contract_id DROP DEFAULT;
       public          postgres    false    225    224            k           2604    16456    token contract_id    DEFAULT     w   ALTER TABLE ONLY public.token ALTER COLUMN contract_id SET DEFAULT nextval('public."token_contractId_seq"'::regclass);
 @   ALTER TABLE public.token ALTER COLUMN contract_id DROP DEFAULT;
       public          postgres    false    220    222    222            l           2604    16457    token project_id    DEFAULT     u   ALTER TABLE ONLY public.token ALTER COLUMN project_id SET DEFAULT nextval('public."token_projectId_seq"'::regclass);
 ?   ALTER TABLE public.token ALTER COLUMN project_id DROP DEFAULT;
       public          postgres    false    221    222    222            m           2604    16508    token token_amount    DEFAULT     y   ALTER TABLE ONLY public.token ALTER COLUMN token_amount SET DEFAULT nextval('public."token_tokenAmount_seq"'::regclass);
 A   ALTER TABLE public.token ALTER COLUMN token_amount DROP DEFAULT;
       public          postgres    false    228    222            g           2604    16432 	   wallet id    DEFAULT     f   ALTER TABLE ONLY public.wallet ALTER COLUMN id SET DEFAULT nextval('public.wallet_id_seq'::regclass);
 8   ALTER TABLE public.wallet ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    217    218            i           2604    16438    wallet contract_id    DEFAULT     y   ALTER TABLE ONLY public.wallet ALTER COLUMN contract_id SET DEFAULT nextval('public."wallet_contractId_seq"'::regclass);
 A   ALTER TABLE public.wallet ALTER COLUMN contract_id DROP DEFAULT;
       public          postgres    false    219    218                      0    16401    contract 
   TABLE DATA           V   COPY public.contract (id, contract_name, contract_address, token_balance) FROM stdin;
    public          postgres    false    216   D@                 0    16461    project 
   TABLE DATA           [   COPY public.project (id, contract_id, project_name, project_description, rent) FROM stdin;
    public          postgres    false    224   �@                 0    16453    token 
   TABLE DATA           F   COPY public.token (contract_id, project_id, token_amount) FROM stdin;
    public          postgres    false    222   aA                 0    16429    wallet 
   TABLE DATA           M   COPY public.wallet (id, name, private_key, contract_id, balance) FROM stdin;
    public          postgres    false    218   �A       +           0    0    contract_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.contract_id_seq', 7, true);
          public          postgres    false    215            ,           0    0    project_contractId_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."project_contractId_seq"', 1, false);
          public          postgres    false    225            -           0    0    project_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.project_id_seq', 2, true);
          public          postgres    false    223            .           0    0    project_rent_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.project_rent_seq', 1, false);
          public          postgres    false    227            /           0    0    token_contractId_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."token_contractId_seq"', 1, false);
          public          postgres    false    220            0           0    0    token_projectId_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."token_projectId_seq"', 1, false);
          public          postgres    false    221            1           0    0    token_tokenAmount_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public."token_tokenAmount_seq"', 1, false);
          public          postgres    false    228            2           0    0    wallet_balance_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.wallet_balance_seq', 1, false);
          public          postgres    false    226            3           0    0    wallet_contractId_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public."wallet_contractId_seq"', 1, false);
          public          postgres    false    219            4           0    0    wallet_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.wallet_id_seq', 3, true);
          public          postgres    false    217            r           2606    16409    contract contract_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.contract
    ADD CONSTRAINT contract_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.contract DROP CONSTRAINT contract_pkey;
       public            postgres    false    216            y           2606    16466    project project_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT project_pkey;
       public            postgres    false    224            w           2606    16459    token token_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.token
    ADD CONSTRAINT token_pkey PRIMARY KEY (contract_id, project_id);
 :   ALTER TABLE ONLY public.token DROP CONSTRAINT token_pkey;
       public            postgres    false    222    222            u           2606    16436    wallet wallet_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.wallet
    ADD CONSTRAINT wallet_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.wallet DROP CONSTRAINT wallet_pkey;
       public            postgres    false    218            s           1259    16450    fki_contractId    INDEX     J   CREATE INDEX "fki_contractId" ON public.wallet USING btree (contract_id);
 $   DROP INDEX public."fki_contractId";
       public            postgres    false    218            z           2606    16445    wallet contractId    FK CONSTRAINT     �   ALTER TABLE ONLY public.wallet
    ADD CONSTRAINT "contractId" FOREIGN KEY (contract_id) REFERENCES public.contract(id) NOT VALID;
 =   ALTER TABLE ONLY public.wallet DROP CONSTRAINT "contractId";
       public          postgres    false    216    4722    218            }           2606    16473    project contractId    FK CONSTRAINT     �   ALTER TABLE ONLY public.project
    ADD CONSTRAINT "contractId" FOREIGN KEY (contract_id) REFERENCES public.contract(id) NOT VALID;
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT "contractId";
       public          postgres    false    4722    224    216            {           2606    16480    token contractId    FK CONSTRAINT     �   ALTER TABLE ONLY public.token
    ADD CONSTRAINT "contractId" FOREIGN KEY (contract_id) REFERENCES public.contract(id) NOT VALID;
 <   ALTER TABLE ONLY public.token DROP CONSTRAINT "contractId";
       public          postgres    false    222    216    4722            |           2606    16485    token projectId    FK CONSTRAINT        ALTER TABLE ONLY public.token
    ADD CONSTRAINT "projectId" FOREIGN KEY (project_id) REFERENCES public.project(id) NOT VALID;
 ;   ALTER TABLE ONLY public.token DROP CONSTRAINT "projectId";
       public          postgres    false    224    4729    222               �   x�]�A� ���a�f�&&�)�����������5��<��"�s�����a��ѫ���)1s�~����rM��T�e��+�m�)�WQ0>B�%.ѳ�2@Y�����_�dB�²ڕ���Rv�i�#��kX���7�?�         i   x�3�4�4SH,K�+MUHIU�IT(:���4)'�(��Y���)VH��;�H[p�p�(�䗦e^Q�
א�W2��(1�(3�Ӏ+F��� 6$         !   x�3�4�44�2�4��2�&\�`v� :z�         t   x�E�1�0F��>[7T;u��X"�JA����>�����)=���'���/�6\|BAp�+H)��]�k*!���YͤQFz���o�J5�k�#TE���IH��|;3�L\%�     